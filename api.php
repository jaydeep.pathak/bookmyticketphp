<?php
header('Access-Control-Allow-Origin: *');
require_once('./dbConnect.php');
require_once('./functions.php');

$res = array();

if (isset($_POST['action'])) {
    $action = $_POST['action'];
    if ($action === 'listAllSeats') {
        $res = listAllSeats($con);
    } else if ($action === 'bookTicket') {
        $res = bookTicket($con, $_POST);
    } else if ($action === 'unBookTicket') {
        $res = unBookTicket($con, $_POST);
    }
} else {
    $res['success'] = false;
    $res['errorMessage'] = 'no parameter passed';
}
echo json_encode($res);
