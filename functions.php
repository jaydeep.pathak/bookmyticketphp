<?php

function listAllSeats($con)
{
    $showId = 'e1367c51-9e26-4d98-afc1-06f739deb5be';
    $sql = "SELECT s.seatName,s.seatId, b.status FROM seats s LEFT JOIN booking b ON s.seatId = b.seatId WHERE s.screenId = (SELECT screenId from shows where showId='$showId') ORDER BY LEFT(s.seatName, 1),CAST(SUBSTR(s.seatName, 2) AS SIGNED)";
    $result = $con->query($sql);

    $data = array();
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $tempArr = array();
            if ($row['status'] === null || $row['status'] === 'available') {
                $tempArr['status'] = 'available';
            } else {
                $tempArr['status'] = 'booked';
            }
            $tempArr['seatName'] = $row['seatName'];
            $tempArr['seatId'] = $row['seatId'];
            array_push($data, $tempArr);
        }
    }
    $res['success'] = true;
    $res['data'] = $data;
    return $res;
}

function bookTicket($con, $post)
{
    $showId = 'e1367c51-9e26-4d98-afc1-06f739deb5be';
    $seatId = $post['seatId'];
    $customerName = $post['customerName'];
    $customerEmail = $post['customerEmail'];
    $uuid = gen_uuid();
    $sql = "INSERT INTO booking(bookingId,showId,seatId,status,customerName,customerEmail) values('$uuid','$showId','$seatId','booked','$customerName','$customerEmail')";
    if ($con->query($sql)) {
        $res['success'] = true;
        $res['data'] = $uuid;
    }
    return $res;
}

function unBookTicket($con, $post)
{
    $bookingId = $post['bookingId'];
    $sql = "DELETE FROM booking WHERE bookingId = '$bookingId' ";
    if ($con->query($sql)) {
        $res['success'] = true;
    }
    return $res;
}

function gen_uuid()
{
    return sprintf(
        '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0x0fff) | 0x4000,
        mt_rand(0, 0x3fff) | 0x8000,
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0xffff)
    );
}
