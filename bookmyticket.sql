-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 01, 2019 at 04:30 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id11365647_bookmyticket`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `bookingId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `showId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `seatId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `customerName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customerEmail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`bookingId`, `showId`, `seatId`, `status`, `customerName`, `customerEmail`) VALUES
('131cf169-e695-4372-87b5-8be134ee5fba', 'e1367c51-9e26-4d98-afc1-06f739deb5be', 'e9bb270a-fb3c-11e9-ac77-feed01180011', 'booked', 'kwer', 'km@kam.com'),
('22cb3d2d-ca0b-4b3a-b154-f935b20a0dee', 'e1367c51-9e26-4d98-afc1-06f739deb5be', 'e9bb2691-fb3c-11e9-ac77-feed01180011', 'booked', 'aksd', 'knndksa@mad.copm'),
('5de896f6-9df8-427e-b4f5-5c415690fc05', 'e1367c51-9e26-4d98-afc1-06f739deb5be', 'e9bb26c0-fb3c-11e9-ac77-feed01180011', 'booked', 'Abcd', 'abcd@aa.com'),
('8d23c227-acef-458f-a9a2-f03f4b85836d', 'e1367c51-9e26-4d98-afc1-06f739deb5be', 'e9bb26e5-fb3c-11e9-ac77-feed01180011', 'booked', 'Test', 'test@abc.com'),
('94e1a0bd-b5d1-43a8-805c-2e25161a6d4d', 'e1367c51-9e26-4d98-afc1-06f739deb5be', '5c6b721b-fb3c-11e9-ac77-feed01180011', 'booked', 'ABCD', 'abcd@test.com'),
('b8e83b0d-1455-405f-94a8-cb17c9161abe', 'e1367c51-9e26-4d98-afc1-06f739deb5be', '5c6b7457-fb3c-11e9-ac77-feed01180011', 'booked', 'asdkn', 'kads@oqeo.com'),
('c49a4b15-a405-4a5b-9fad-ee46f4cae763', 'e1367c51-9e26-4d98-afc1-06f739deb5be', 'e9bb2390-fb3c-11e9-ac77-feed01180011', 'booked', 'panda', 'panda@gmail.com'),
('cac8a49f-c83f-4f48-8272-35728d8745c0', 'e1367c51-9e26-4d98-afc1-06f739deb5be', 'e9bb2614-fb3c-11e9-ac77-feed01180011', 'booked', 'asd', 'nkasd@kad.com');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `movieId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `movieName` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `movieGenre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `movieCertificate` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `movieDescription` varchar(10000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`movieId`, `movieName`, `movieGenre`, `movieCertificate`, `movieDescription`) VALUES
('d62f9948-703e-49dd-a27b-743bd7ef3ded', 'Avengers : Endgame', 'superhero', 'ua', 'Twenty-three days after Thanos used the Infinity Gauntlet to disintegrate half of all life in the universe,[N 1] Carol Danvers rescues Tony Stark and Nebula from deep space and returns them to Earth, where they reunite with the remaining Avengers—Bruce Banner, Steve Rogers, Thor, Natasha Romanoff, and James Rhodes—and Rocket. Locating Thanos on an otherwise uninhabited planet, they plan to retake and use the Infinity Stones to reverse \"the Snap\", but Thanos reveals he destroyed the Stones to prevent their further use. Enraged, Thor decapitates Thanos.');

-- --------------------------------------------------------

--
-- Table structure for table `screen`
--

CREATE TABLE `screen` (
  `screenId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `screenName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `screenDescription` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `screen`
--

INSERT INTO `screen` (`screenId`, `screenName`, `screenDescription`) VALUES
('1bd72670-fb3c-11e9-ac77-feed01180011', 'Audi-1', 'Dolby Atmos');

-- --------------------------------------------------------

--
-- Table structure for table `seats`
--

CREATE TABLE `seats` (
  `seatId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `seatName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `screenId` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seats`
--

INSERT INTO `seats` (`seatId`, `seatName`, `screenId`) VALUES
('5c6b721b-fb3c-11e9-ac77-feed01180011', 'A1', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('5c6b7457-fb3c-11e9-ac77-feed01180011', 'A2', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2390-fb3c-11e9-ac77-feed01180011', 'A3', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2614-fb3c-11e9-ac77-feed01180011', 'A4', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2691-fb3c-11e9-ac77-feed01180011', 'A5', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb26c0-fb3c-11e9-ac77-feed01180011', 'A6', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb26e5-fb3c-11e9-ac77-feed01180011', 'A7', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb270a-fb3c-11e9-ac77-feed01180011', 'A8', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2737-fb3c-11e9-ac77-feed01180011', 'A9', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2757-fb3c-11e9-ac77-feed01180011', 'A10', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2778-fb3c-11e9-ac77-feed01180011', 'B1', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2798-fb3c-11e9-ac77-feed01180011', 'B2', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb27b9-fb3c-11e9-ac77-feed01180011', 'B3', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb27e2-fb3c-11e9-ac77-feed01180011', 'B4', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb27ff-fb3c-11e9-ac77-feed01180011', 'B5', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2823-fb3c-11e9-ac77-feed01180011', 'B6', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2843-fb3c-11e9-ac77-feed01180011', 'B7', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2861-fb3c-11e9-ac77-feed01180011', 'B8', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb287e-fb3c-11e9-ac77-feed01180011', 'B9', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb292f-fb3c-11e9-ac77-feed01180011', 'B10', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb294e-fb3c-11e9-ac77-feed01180011', 'C1', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb296c-fb3c-11e9-ac77-feed01180011', 'C2', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2988-fb3c-11e9-ac77-feed01180011', 'C3', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb29a4-fb3c-11e9-ac77-feed01180011', 'C4', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb29c0-fb3c-11e9-ac77-feed01180011', 'C5', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb29dd-fb3c-11e9-ac77-feed01180011', 'C6', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb29f9-fb3c-11e9-ac77-feed01180011', 'C7', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2a14-fb3c-11e9-ac77-feed01180011', 'C8', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2a32-fb3c-11e9-ac77-feed01180011', 'C9', '1bd72670-fb3c-11e9-ac77-feed01180011'),
('e9bb2a4f-fb3c-11e9-ac77-feed01180011', 'C10', '1bd72670-fb3c-11e9-ac77-feed01180011');

-- --------------------------------------------------------

--
-- Table structure for table `shows`
--

CREATE TABLE `shows` (
  `showId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `screenId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `movieId` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shows`
--

INSERT INTO `shows` (`showId`, `date`, `time`, `screenId`, `movieId`) VALUES
('e1367c51-9e26-4d98-afc1-06f739deb5be', '2019-10-30', '12:00:00', '1bd72670-fb3c-11e9-ac77-feed01180011', 'd62f9948-703e-49dd-a27b-743bd7ef3ded');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`bookingId`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`movieId`);

--
-- Indexes for table `screen`
--
ALTER TABLE `screen`
  ADD PRIMARY KEY (`screenId`);

--
-- Indexes for table `seats`
--
ALTER TABLE `seats`
  ADD PRIMARY KEY (`seatId`);

--
-- Indexes for table `shows`
--
ALTER TABLE `shows`
  ADD PRIMARY KEY (`showId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
